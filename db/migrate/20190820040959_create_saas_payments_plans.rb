class CreateSaasPaymentsPlans < ActiveRecord::Migration[5.2]
  def change
    create_table :saas_payments_plans do |t|
      # String ids from stripe
      t.string :stripe_id
      t.string :product_id

      t.boolean :active
      t.integer :amount
      t.string :amount_decimal
      t.string :currency
      t.string :interval
      t.integer :interval_count
      t.boolean :livemode
      t.text :metadata # hash
      t.string :nickname
      t.integer :trial_period_days

      t.timestamps
    end

    add_index :saas_payments_plans, :stripe_id, unique: true
    add_index :saas_payments_plans, [:stripe_id, :product_id], unique: true
  end
end
