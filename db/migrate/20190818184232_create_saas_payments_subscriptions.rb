class CreateSaasPaymentsSubscriptions < ActiveRecord::Migration[5.2]
  def change
    create_table :saas_payments_subscriptions do |t|
      # String ids from stripe
      t.string :plan_id
      t.string :customer_id
      t.string :stripe_id

      t.datetime :cancel_at
      t.datetime :canceled_at
      t.datetime :current_period_end
      t.datetime :current_period_start
      t.boolean :cancel_at_period_end
      t.boolean :livemode
      t.text :metadata
      t.datetime :start
      t.datetime :start_date
      t.string :status
      t.datetime :trial_end
      t.datetime :trial_start

      t.timestamps
    end

    add_index :saas_payments_subscriptions, :stripe_id, unique: true
    add_index :saas_payments_subscriptions, :customer_id
  end
end
