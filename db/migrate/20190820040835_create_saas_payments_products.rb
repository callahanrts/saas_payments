class CreateSaasPaymentsProducts < ActiveRecord::Migration[5.2]
  def change
    create_table :saas_payments_products do |t|
      t.string :stripe_id

      t.boolean :active
      t.string :caption
      t.boolean :livemode
      t.text :metadata
      t.string :name
      t.string :type
      t.string :unit_label

      t.timestamps
    end

    add_index :saas_payments_products, :stripe_id, unique: true
  end
end
