class CreateSaasPaymentsCustomers < ActiveRecord::Migration[5.2]
  def change
    create_table :saas_payments_customers do |t|
      t.belongs_to :user
      t.string :stripe_id
      t.boolean :delinquent
      t.string :description
      t.text :discount
      t.string :email
      t.boolean :livemode
      t.text :metadata
      t.string :name

      t.timestamps
    end

    add_index :saas_payments_customers, :stripe_id, unique: true
  end
end
