# SaasPayments

SaasPayments is a thin wrapper around the Stripe API to manage the most common
SAAS subscription actions. This gem includes

- [ ] Subscription model to hold the state of a user's subscription
- [ ] Payment form template
- [ ] Webhook to handle subscription updates from Stripe
- [ ] Sign up, cancel, and change plan routes


This Gem also assumes you already have:

- A `User` model (If you don't, try [Devise](https://github.com/plataformatec/devise))

# Getting Started

## Install
Add this line to your application's Gemfile:

```ruby
# Gemfile

gem 'saas_payments'
```

And then run:
```bash
bundle install
```

## Migrations
```bash
rake saas_payments:install:migrations
rails db:migrate
```

## Configuration

```ruby
SaasPayments.configure do |config|
  # Stripe API Keys
  config.stripe_secret_key = "sk_...."
  config.stripe_publishable_key = "pk_..."

  # Path to return to after successful subscription changes
  config.account_path = '/dashboard'
end
```

## Importing data from Stripe

### Rake task to import Products/Plans
```bash
rake products:sync
```

## Managing Subscriptions

Subscriptions are managed using a set of controller concerns. Subscriptions
can be created and modified by including the `SaasPayments::Subscriptions`
concern into your controller.

### Creating subscriptions

Currently, only one subscription is supported per user. Should a user enter
their card information more than once, their current subscription will be
updated, rather than creating a second subscription.

```ruby
class AccountsController < ApplicationController
  # Include the subscriptions concern
  include SaasPayments::Subscriptions

  def create
    sign_up_for_plan @current_user, @plan
  end

end
```

### Cancellation

NOTE: Canceled subscriptions will be canceled at the end of the period.

```ruby
# Host application
class AccountsController < ApplicationController
  # Include the subscriptions concern
  include SaasPayments::Subscriptions

  def cancel
    # Perform any logic you need

    # Call the cancel_subscription method to perform the
    # cancellation
    cancel_at_period_end @current_user
  end
end
```

### Changing Plans

The subscriptions concern also contains a method for changing plans.

```ruby
# Host application
class AccountsController < ApplicationController
  # Include the subscriptions concern
  include SaasPayments::Subscriptions

  def change_plan
    # Perform any logic you need

    # Call the change_plan method to change the plan a user is
    # subscribed to.
    change_plan @current_user, @plan
  end
end
```

## Development

- Use `bin/rails`. For example, generators using this executable will create
  models/controllers/etc that are namespaced to the saas_payments gem


## Testing ([Rails Guide](https://guides.rubyonrails.org/engines.html#testing-an-engine))
```bash
make test
```

## Contributing
Contribution directions go here.


## License
The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).


## TODO

- [ ] Per unit pricing
- [ ] Tiered pricing
- [ ] Metered pricing
- [ ] Product pricing
