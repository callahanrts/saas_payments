SaasPayments::Engine.routes.draw do
  get '/', to: 'application#index'
  resources :charge, only: [:create]
end
