RAILS="./bin/rails"

.PHONY: test routes

test:
	@$(RAILS) test

routes:
	@rake app:routes

build:
	@gem build saas_payments.gemspec

release-patch:
	@./scripts/release patch

release-minor:
	@./scripts/release minor

release-major:
	@./scripts/release major
