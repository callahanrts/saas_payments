namespace :products do
  desc "Create products in the database that come from stripe"
  task sync: :environment do
    SaasPayments::ProductsService.sync
  end
end
