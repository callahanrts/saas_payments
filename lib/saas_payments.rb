require "saas_payments/engine"
require "saas_payments/config"

module SaasPayments
  require 'stripe'

  class << self
    attr_reader :config

    def configure
      @config = Configuration.new
      yield config
    end
  end
end
