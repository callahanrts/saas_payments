require_relative './errors'

module SaasPayments
  class Engine < ::Rails::Engine
    isolate_namespace SaasPayments
  end

  def self.included(base)
    base.extend(ClassMethods)
  end

  module ClassMethods
    def configure
      yield(self)
    end
  end

end
