
module SaasPayments
  class Configuration
    attr_accessor :stripe_secret_key
    attr_accessor :stripe_publishable_key
    attr_accessor :account_path
    attr_accessor :webhook_secret
  end
end
