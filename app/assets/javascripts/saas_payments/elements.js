'use strict';

document.addEventListener('DOMContentLoaded', function() {

  var Card = function(stripe) {
    // Create an instance of Elements.
    var elements = stripe.elements();

    // Custom styling can be passed to options when creating an Element.
    // (Note that this demo uses a wider set of styles than the guide below.)
    var style = {
      base: {
        color: '#32325d',
        fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
        fontSmoothing: 'antialiased',
        fontSize: '16px',
        '::placeholder': {
          color: '#aab7c4'
        }
      },
      invalid: {
        color: '#fa755a',
        iconColor: '#fa755a'
      }
    };

    // Create an instance of the card Element.
    var card = elements.create('card', {style: style});

    // Handle real-time validation errors from the card Element.
    card.addEventListener('change', function(event) {
      var displayError = document.getElementById('card-errors');
      if (event.error) {
        displayError.textContent = event.error.message;
      } else {
        displayError.textContent = '';
      }
    });

    return card
  }

  var SPElements = function() {
    // Retrieve the publishable key
    var key = document.getElementById("publishable-key").dataset.publishable

    // Create a Stripe client.
    var stripe = Stripe(key);

    var options = options || {}
    var card = Card(stripe);
    var forms = document.getElementsByClassName('payment-form')

    var init = function() {
      for (var i = 0; i < forms.length; i++) {
        initForm(forms[i])
      }
      if (forms.length === 1) {
        this.show(forms[0].id)
      }
    }

    // Submit the form with the token ID.
    var stripeTokenHandler = function(form, token) {
      // Insert the token ID into the form so it gets submitted to the server
      var hiddenInput = document.createElement('input');
      hiddenInput.setAttribute('type', 'hidden');
      hiddenInput.setAttribute('name', 'stripeToken');
      hiddenInput.setAttribute('value', token.id);
      form.appendChild(hiddenInput);

      // Submit the form
      form.submit();
    }

    var initForm = function(form) {
      // Handle form submission.
      form.addEventListener('submit', onSubmit.bind(this, form))
    }

    var onSubmit = function(form, event) {
      if (event) {
        event.preventDefault();
      }

      stripe.createToken(card).then(function(result) {
        if (result.error) {
          // Inform the user if there was an error.
          var errorElement = document.getElementById('card-errors');
          errorElement.textContent = result.error.message;
        } else {
          // Send the token to your server.
          stripeTokenHandler(form, result.token);
        }
      });
    }

    var hideAll = function() {
      var forms = document.getElementsByClassName('payment-form')
      for (var i = 0; i < forms.length; i++) {
        forms[i].style.display = 'none'
      }
    }

    var setHiddenValue = function(form, className, value) {
      var field = form.getElementsByClassName(className)[0]
      field.value = value
    }

    this.show = function(id) {
      this.form = document.getElementById(id)

      if(!this.form) {
        console.error("Could not find form with id ("+id+")")
        return
      }

      hideAll()
      this.form.style.display = ''
      card.unmount()
      card.mount('#' + id + "_card") // Add an instance of the card Element into the `card-element` <div>.
    }

    this.setData = function(options) {
      if(!this.form) {
        console.error("Form must be shown first")
        return
      }

      var defaults = ['plan_id', 'user_id']
      for (var i = 0; i < defaults.length; i++) {
        var key = defaults[i]
        if(options[key]) setHiddenValue(this.form, 'form_'+key, options[key])
      }
    }

    this.submit = function() {
      onSubmit(this.form)
    }

    init.call(this)
  }

  window.SPElements = SPElements
})

