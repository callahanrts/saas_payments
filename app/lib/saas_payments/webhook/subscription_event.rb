module SaasPayments
  class Webhook::SubscriptionEvent
    def created data
      Subscription.create!(Subscription.from_stripe(data))
    end

    def updated data
      sub = Subscription.find_by_stripe_id(data[:id])
      raise_not_found(data[:id]) unless sub
      sub.update(Subscription.from_stripe(data))
    end

    def deleted data
      sub = Subscription.find_by_stripe_id(data[:id])
      raise_not_found(data[:id]) unless sub
      sub.delete
    end

    private

    def raise_not_found id
      raise ActiveRecord::RecordNotFound.new("Could not find subscription with stripe id: #{id}")
    end
  end
end
