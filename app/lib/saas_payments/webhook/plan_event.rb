module SaasPayments
  class Webhook::PlanEvent
    def created data
      Plan.create!(Plan.from_stripe(data))
    end

    def updated data
      plan = Plan.find_by_stripe_id(data[:id])
      raise_not_found(data[:id]) unless plan
      plan.update(Plan.from_stripe(data))
    end

    def deleted data
      plan = Plan.find_by_stripe_id(data[:id])
      raise_not_found(data[:id]) unless plan
      plan.delete
    end

    private

    def raise_not_found id
      raise ActiveRecord::RecordNotFound.new("Could not find plan with stripe id: #{id}")
    end
  end
end
