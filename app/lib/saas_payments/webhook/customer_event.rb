module SaasPayments
  class Webhook::CustomerEvent
    def created data
      Customer.create(Customer.from_stripe(data).merge({ user_id: user_id(data) }))
    end

    def updated data
      customer = Customer.find_by_stripe_id(data[:id])
      raise_not_found(data[:id]) unless customer
      customer.update(Customer.from_stripe(data))
    end

    def deleted data
      customer = Customer.find_by_stripe_id(data[:id])
      raise_not_found(data[:id]) unless customer
      customer.delete
    end

    private

    def user_id data
      data[:metadata][:user_id] rescue nil
    end

    def raise_not_found id
      raise ActiveRecord::RecordNotFound.new("Could not find customer with stripe id: #{id}")
    end
  end
end
