module SaasPayments
  class Webhook::ProductEvent
    def created data
      Product.create!(Product.from_stripe(data))
    end

    def updated data
      product = Product.find_by_stripe_id(data[:id])
      raise_not_found(data[:id]) unless product
      product.update(Product.from_stripe(data))
    end

    def deleted data
      product = Product.find_by_stripe_id(data[:id])
      raise_not_found(data[:id]) unless product
      product.delete
    end

    private

    def raise_not_found id
      raise ActiveRecord::RecordNotFound.new("Could not find product with stripe_id: #{id}")
    end
  end
end
