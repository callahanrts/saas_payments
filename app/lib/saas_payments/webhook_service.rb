module SaasPayments
  class WebhookService
    EVENTS = {
      # Register Product events
      'product.created' => [Webhook::ProductEvent, 'created'],
      'product.deleted' => [Webhook::ProductEvent, 'deleted'],
      'product.updated' => [Webhook::ProductEvent, 'updated'],

      # Register Plan events
      'plan.created' => [Webhook::PlanEvent, 'created'],
      'plan.deleted' => [Webhook::PlanEvent, 'deleted'],
      'plan.updated' => [Webhook::PlanEvent, 'updated'],

      # Register Customer events
      'customer.created' => [Webhook::CustomerEvent, 'created'],
      'customer.deleted' => [Webhook::CustomerEvent, 'deleted'],
      'customer.updated' => [Webhook::CustomerEvent, 'updated'],

      # Register Subscription events
      'customer.subscription.created' => [Webhook::SubscriptionEvent, 'created'],
      'customer.subscription.deleted' => [Webhook::SubscriptionEvent, 'deleted'],
      'customer.subscription.updated' => [Webhook::SubscriptionEvent, 'updated'],

      # Register Session events
      'checkout.session.completed' => [Webhook::SessionEvent, 'completed']
    }

    def initialize event
      @event = event
    end

    def process
      event, action = EVENTS[@event[:type]]
      raise WebhookError.new("Unrecognized event") unless event && action
      event.new.send(action, @event[:data][:object])
    end

  end
end
