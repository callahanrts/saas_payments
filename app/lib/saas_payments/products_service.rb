module SaasPayments
  class ProductsService
    def self.sync
      Stripe.api_key = SaasPayments.config.stripe_secret_key
      Stripe::Product.all.each{ |p| create_product p }
      Stripe::Plan.list.each{ |p| create_plan p }
    end

    private

    def self.create_product product
      Product.create!(Product.from_stripe(product))
    rescue ActiveRecord::RecordInvalid
      puts "Product (#{product[:id]}) already exists. Updating..."
      Product.find_by_stripe_id(product[:id]).update!(Product.from_stripe(product))
    end

    def self.create_plan plan
      Plan.create!(Plan.from_stripe(plan))
    rescue ActiveRecord::RecordInvalid
      puts "Plan (#{plan[:id]}) already exists. Updating..."
      Plan.find_by_stripe_id(plan[:id]).update!(Plan.from_stripe(plan))
    end
  end
end
