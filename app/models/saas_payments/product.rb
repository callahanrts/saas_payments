module SaasPayments
  class Product < ApplicationRecord
    include StripeModel

    validates_uniqueness_of :stripe_id

    has_many :plans

    serialize :metadata

    def self.from_stripe p
      {
        stripe_id:             p[:id],
        active:                p[:active],
        caption:               p[:caption],
        livemode:              truthy?(p[:livemode]),
        metadata:              stripe_hash(p[:metadata]),
        name:                  p[:name],
        unit_label:            p[:unit_label],
      }
    end

  end
end
