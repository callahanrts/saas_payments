module SaasPayments
  class Subscription < ApplicationRecord
    include StripeModel

    validates_uniqueness_of :stripe_id

    serialize :metadata

    def self.from_stripe s
      {
        plan_id:               parse_id(s[:plan]),
        customer_id:           parse_id(s[:customer]),

        stripe_id:             s[:id],
        cancel_at:             date(s[:cancel_at]),
        canceled_at:           date(s[:canceled_at]),
        current_period_end:    date(s[:current_period_end]),
        current_period_start:  date(s[:current_period_start]),
        cancel_at_period_end:  truthy?(s[:cancel_at_period_end]),
        livemode:              truthy?(s[:livemode]),
        metadata:              stripe_hash(s[:metadata]),
        start:                 date(s[:start]),
        start_date:            date(s[:start_date]),
        status:                s[:status], # TODO: Status might not come from stripe?
        trial_end:             date(s[:trial_end]),
        trial_start:           date(s[:trial_start])
      }
    end

  end
end
