module SaasPayments
  class Plan < ApplicationRecord
    include StripeModel

    validates_uniqueness_of :stripe_id

    serialize :metadata

    def self.from_stripe p
      {
        product_id:         parse_id(p[:product]),

        stripe_id:          p[:id],
        active:             p[:active],
        amount:             p[:amount].to_i,
        amount_decimal:     p[:amount_decimal],
        currency:           p[:currency],
        interval:           p[:interval],
        interval_count:     p[:interval_count].to_i,
        livemode:           truthy?(p[:livemode]),
        metadata:           stripe_hash(p[:metadata]),
        nickname:           p[:nickname],
        trial_period_days:  p[:trial_period_days].to_i
      }

    end
  end
end
