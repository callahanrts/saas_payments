module SaasPayments
  class Customer < ApplicationRecord
    include StripeModel

    validates_uniqueness_of :stripe_id

    belongs_to :user, optional: true

    serialize :discount
    serialize :metadata

    def self.from_stripe c
      {
        stripe_id: c[:id],
        delinquent: truthy?(c[:delinquent]),
        description: c[:description],
        discount: stripe_hash(c[:discount]),
        email: c[:email],
        livemode: truthy?(c[:livemode]),
        metadata: stripe_hash(c[:metadata]),
        name: c[:name]
      }
    end

    def subscription options={}
      sub = Subscription.where(customer_id: stripe_id).first
      if sub.present? && options[:remote]
        sub = Stripe::Subscription.retrieve(sub.stripe_id)
      end
      sub
    rescue Stripe::InvalidRequestError => e
      raise e unless /no such/i =~ e.message
      nil
    end

    def plan
      return nil if !subscription.present?
      Plan.find_by_stripe_id(subscription.plan_id)
    end

  end
end
