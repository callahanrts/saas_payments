module SaasPayments
  module StripeModel
    extend ActiveSupport::Concern

    module ClassMethods
      def truthy? val
        val.present? ? val : false
      end

      def stripe_hash data
        (data || {}).to_hash
      end

      def date ts
        ts.present? ? Time.at(ts).to_datetime : nil
      rescue
        nil
      end

      def parse_id val
        return nil unless val

        if val.is_a?(String)
          val
        else
          val[:id]
        end
      end
    end

  end
end
