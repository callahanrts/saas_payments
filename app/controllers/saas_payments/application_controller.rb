module SaasPayments
  class ApplicationController < ActionController::Base
    protect_from_forgery with: :exception
    before_action :init_stripe

    def index
      head :ok
    end

    private

    def init_stripe
      Stripe.api_key = SaasPayments.config.stripe_secret_key || ENV["STRIPE_SECRET_KEY"]
    end

  end
end
