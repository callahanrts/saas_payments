module SaasPayments
  module SubscriptionConcerns
    Stripe.api_key = SaasPayments.config.stripe_secret_key

    class FailedPayment < StandardError; end
    class ActionRequired < StandardError; end

    extend ActiveSupport::Concern

    def sign_up_for_plan user, plan
      customer = get_customer user
      sub = update_subscription customer, plan
      render_success
    rescue ActionRequired
      render_action_required
    rescue FailedPayment
      render_card_failure
    rescue Stripe::CardError
      render_card_failure
    end

    def change_plan user, plan
      customer = get_customer user
      sub = customer.subscription(remote: true)
      update_plan sub, customer, plan
      render_success
    rescue StandardError
      render_error
    end

    def cancel_at_period_end user
      customer = get_customer user
      sub = customer.subscription(remote: true)

      # Customer has an existing subscription, change plans
      remote_sub = Stripe::Subscription.update(sub[:id], {
        cancel_at_period_end: true
      })

      customer.subscription.update(Subscription.from_stripe(remote_sub))
      render_success
    rescue StandardError
      render_error
    end

    def resume_subscription user
      customer = get_customer user
      sub = customer.subscription
      remote_sub = Stripe::Subscription.update(sub.stripe_id, {
        cancel_at_period_end: false
      })

      customer.subscription.update(Subscription.from_stripe(remote_sub))
      render_success
    rescue StandardError
      render_error
    end

    def cancel_now user
      customer = get_customer user
      Stripe::Subscription.delete(customer.subscription.stripe_id)
    end


    private

    def render_success
      respond_to do |format|
        format.html { redirect_to SaasPayments.config.account_path }
        format.json { render json: { message: "success" } }
      end
    end

    def render_error
      format.html {
        flash[:sp_error] = 'Failed to update plan'
        redirect_back fallback_location: root_path
      }
      format.json { render json: { message: "update_failed" }, status: :bad_request }
    end

    def render_action_required
      respond_to do |format|
        format.html {
          flash[:sp_notice] = 'Payment requires customer action'
          redirect_to SaasPayments.successPath
        }
        format.json { render json: { message: "action_required" } }
      end
    end

    def render_card_failure
      respond_to do |format|
        format.html {
          flash[:sp_error] = 'Failed to process card'
          redirect_back fallback_location: root_path
        }
        format.json { render json: { message: "card_error" }, status: :bad_request }
      end
    end

    def get_customer user
      # Try to find a customer in the database by the user id
      customer = SaasPayments::Customer.find_by_user_id(user.id)

      # If no customer exists, create a new one
      if !customer.present?
        # Create a new customer in stripe
        c = Stripe::Customer.create({
          email: user.email,
          source: params.require(:stripeToken),
          metadata: { user_id: user.id }
        })

        # Create a new customer locally that maps to the stripe customer
        customer = Customer.create(Customer.from_stripe(c).merge({ user_id: user.id }))
      end
      customer
    end

    def update_subscription customer, plan
      sub = customer.subscription(remote: true)
      if sub.present?
        update_plan sub, customer, plan
      else
        sub = create_subscription customer, plan
      end

      sub
    end

    def update_plan sub, customer, plan
      # Customer has an existing subscription, change plans
      remote_sub = Stripe::Subscription.update(sub[:id], {
        cancel_at_period_end: false,
        items: [{
          id: sub[:items][:data][0][:id],
          plan: plan.stripe_id
        }]
      })

      customer.subscription.update(Subscription.from_stripe(remote_sub))
    end

    def create_subscription customer, plan
      # Customer does not have a subscription, sign them up
      sub = Stripe::Subscription.create({
        customer: customer.stripe_id,
        items: [{ plan: plan.stripe_id }]
      })
      complete_subscription sub
    end

    def complete_subscription sub
      if incomplete?(sub)
        raise FailedPayment if payment_intent(sub, :requires_payment_method)
        raise ActionRequired if payment_intent(sub, :requires_action)
      end

      # Successful payment, or trialling: provision the subscription
      # Create a local subscription
      Subscription.create(Subscription.from_stripe(sub))
    end

    def incomplete? sub
      sub[:status] == "incomplete"
    end

    def payment_intent sub, intent
      sub[:latest_invoice][:payment_intent][:status] == intent.to_s
    end
  end
end
