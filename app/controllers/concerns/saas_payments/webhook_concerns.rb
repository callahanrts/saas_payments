module SaasPayments
  module WebhookConcerns
    extend ActiveSupport::Concern
    Stripe.api_key = SaasPayments.config.stripe_secret_key
    SECRET = SaasPayments.config.webhook_secret

    def webhook
      event = params
      if SaasPayments.config.webhook_secret.present?
        header = request.headers['HTTP_STRIPE_SIGNATURE']
        event = Stripe::Webhook.construct_event(request.body.read, header, SECRET)
      end
      WebhookService.new(event).process
      head :ok
    rescue ActiveRecord::RecordNotFound
      head :not_found
    rescue StandardError => e
      head :bad_request
    end

  end
end
