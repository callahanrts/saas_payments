VERSION = "lib/saas_payments/version.rb"

bump = ARGV[0]

major = nil
minor = nil
patch = nil
version = nil

File.open(VERSION, "r") do |f|
  f.each_line do |line|
    # TODO: Add a parameter and bump the correct version
    if /VERSION/ =~ line
      result = line.match(/(\d+)\.(\d+)\.(\d+)/)
      major = result[1].to_i
      minor = result[2].to_i
      patch = result[3].to_i
    end

  end
end

def module_code version
"""module SaasPayments
  VERSION = '#{version}'
end
"""
end


if !major.nil? && !minor.nil? && !patch.nil?
  if bump == "major"
    major += 1
    minor = 0
    patch = 0
  elsif bump == "minor"
    minor += 1
    patch = 0
  elsif bump == "patch"
    patch += 1
  end

  version = "#{major}.#{minor}.#{patch}"
  File.open(VERSION, 'w') do |file|
    file.puts module_code(version)
  end
end

version
