FactoryBot.define do
  factory :product, class: SaasPayments::Product
  factory :plan, class: SaasPayments::Plan
  factory :subscription, class: SaasPayments::Subscription
  factory :customer, class: SaasPayments::Customer

  # From the dummy app
  factory :user
end
