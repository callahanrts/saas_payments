require 'simplecov'
SimpleCov.start

require 'stripe_mock'
require 'database_cleaner'
require 'factory_bot'

# Configure Rails Environment
ENV["RAILS_ENV"] = "test"

require_relative "../test/dummy/config/environment"
ActiveRecord::Migrator.migrations_paths = [File.expand_path("../test/dummy/db/migrate", __dir__)]
ActiveRecord::Migrator.migrations_paths << File.expand_path('../db/migrate', __dir__)
require "rails/test_help"


# Filter out Minitest backtrace while allowing backtrace from other libraries
# to be shown.
Minitest.backtrace_filter = Minitest::BacktraceFilter.new

DatabaseCleaner.strategy = :truncation
FactoryBot.find_definitions

class ActiveSupport::TestCase
  include FactoryBot::Syntax::Methods

  setup do
    StripeMock.start
    DatabaseCleaner.start
  end

  teardown do
    StripeMock.stop
    DatabaseCleaner.clean
  end

  # Add more helper methods to be used by all tests here...

  def stripe_api_key
    ENV["STRIPE_SECRET_KEY"]
  end

  def stripe_helper
    StripeMock.create_test_helper
  end

  def total_customers
    SaasPayments::Customer.all.size
  end

  def total_plans
    SaasPayments::Plan.all.size
  end

  def total_products
    SaasPayments::Product.all.size
  end

  def total_subs
    SaasPayments::Subscription.all.size
  end

end
