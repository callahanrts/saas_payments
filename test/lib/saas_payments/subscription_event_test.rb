require 'test_helper'

module SaasPayments
  class SubscriptionEventTest < ActiveSupport::TestCase

    def created_event
      StripeMock.mock_webhook_event('customer.subscription.created', {
        metadata: { user_id: 1 }
      })
    end

    test 'customer.subscription.created should create a sub' do
      subs = total_subs
      Webhook::SubscriptionEvent.new.created created_event[:data][:object]
      assert_equal subs + 1, total_subs
    end

    test 'customer.subscription.updated should update a sub' do
      data = created_event[:data][:object]
      sub = create(:subscription, stripe_id: data[:id])

      # Change the livemode value
      data[:livemode] = !data[:livemode]

      # Update the sub and assert the change
      Webhook::SubscriptionEvent.new.updated data
      assert_equal data[:livemode], sub.reload.livemode

      data[:cancel_at] = DateTime.now.to_i

      # Update the sub and assert the change
      Webhook::SubscriptionEvent.new.updated data
      assert_equal data[:cancel_at], sub.reload.cancel_at.to_i
    end

    test 'subscription dates should be updated' do
      data = created_event[:data][:object]
      sub = create(:subscription, stripe_id: data[:id])
    end

    test 'customer.subscription.deleted should remove a sub' do
      data = created_event[:data][:object]
      c = create(:subscription, stripe_id: data[:id])

      # Keep track of the current number of subs
      subs = total_subs

      # Send the delete event
      Webhook::SubscriptionEvent.new.deleted data

      # Assert that there is one less
      assert_equal subs - 1, total_subs
    end
  end
end

