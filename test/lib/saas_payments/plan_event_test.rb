require 'test_helper'

module SaasPayments
  class PlanEventTest < ActiveSupport::TestCase

    def created_event
      StripeMock.mock_webhook_event('plan.created', {
        metadata: { user_id: 1 }
      })
    end

    test 'plan.created should create a plan' do
      plans = total_plans
      Webhook::PlanEvent.new.created created_event[:data][:object]
      assert_equal plans + 1, total_plans
    end

    test 'plan.updated should update a plan' do
      data = created_event[:data][:object]
      plan = create(:plan, stripe_id: data[:id])

      # Change the livemode value
      data[:livemode] = !data[:livemode]

      # Update the plan and assert the change
      Webhook::PlanEvent.new.updated data
      assert_equal data[:livemode], plan.reload.livemode
    end

    test 'plan.deleted should remove a plan' do
      data = created_event[:data][:object]
      c = create(:plan, stripe_id: data[:id])

      # Keep track of the current number of plans
      plans = total_plans

      # Send the delete event
      Webhook::PlanEvent.new.deleted data

      # Assert that there is one less
      assert_equal plans - 1, total_plans
    end
  end
end

