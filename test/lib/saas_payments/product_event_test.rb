require 'test_helper'

module SaasPayments
  class ProductEventTest < ActiveSupport::TestCase
    def created_event
      StripeMock.mock_webhook_event('product.created', {
        metadata: { user_id: 1 }
      })
    end

    test 'product.created should create a product' do
      products = total_products
      Webhook::ProductEvent.new.created created_event[:data][:object]
      assert_equal products + 1, total_products
    end

    test 'product.updated should update a product' do
      data = created_event[:data][:object]
      product = create(:product, stripe_id: data[:id])

      # Change the livemode value
      data[:livemode] = !data[:livemode]

      # Update the product and assert the change
      Webhook::ProductEvent.new.updated data
      assert_equal data[:livemode], product.reload.livemode
    end

    test 'product.deleted should remove a product' do
      data = created_event[:data][:object]
      c = create(:product, stripe_id: data[:id])

      # Keep track of the current number of products
      products = total_products

      # Send the delete event
      Webhook::ProductEvent.new.deleted data

      # Assert that there is one less
      assert_equal products - 1, total_products
    end
  end
end

