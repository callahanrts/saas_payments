require 'test_helper'

module SaasPayments
  class ProductsServiceTest < ActiveSupport::TestCase
    setup do
      @products = [
        Stripe::Product.create({ name: 'Mead', type: 'good', active: true }),
        Stripe::Product.create({ name: 'Mead Sub', type: 'service', active: true })
      ]

      @plans = [
        Stripe::Plan.create({
          product: @products[1][:id],
          amount: 999,
          currency: 'usd',
          interval: 'month'
        }),

        Stripe::Plan.create({
          product: @products[1][:id],
          amount: 1999,
          currency: 'usd',
          interval: 'month'
        })
      ]
    end

    teardown do
      @products.each(&:delete)
      @plans.each(&:delete)
    end

    test "should create products from stripe" do
      ProductsService.sync
      saved_products = Product.all.sort_by{|h| h[:name]}

      assert_equal @products.size, saved_products.size
      @products.sort_by{|p| p[:name]}.each_with_index do |p, i|
        assert_equal p.name, saved_products[i].name
      end
    end

    test 'should create plans from stripe' do
      ProductsService.sync

      @plans.each do |p|
        assert_not_nil Plan.find_by_stripe_id(p[:id])
      end
    end

    test 'should update plans from stripe' do
      ProductsService.sync

      @plans.each do |p|
        Stripe::Plan.update(p[:id], metadata: { public: true })
      end

      ProductsService.sync
      Plan.all.each do |p|
        assert_equal true, p.metadata[:public]
      end
    end
  end
end
