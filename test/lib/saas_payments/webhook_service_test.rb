require 'test_helper'

module SaasPayments
  class WebhookServiceTest < ActiveSupport::TestCase
    test "Should raise an error with unknown events" do
      event = { type: "not.a.real.event" }
      assert_raise WebhookError do
        WebhookService.new(event).process
      end
    end

    test "Should test webhooks" do
      event = StripeMock.mock_webhook_event('customer.created')
      WebhookService.new(event).process
      assert true
    end
  end
end
