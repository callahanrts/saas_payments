require 'test_helper'

module SaasPayments
  class CustomerEventTest < ActiveSupport::TestCase
    setup do
      @user = create(:user)
    end

    def created_event
      StripeMock.mock_webhook_event('customer.created', {
        metadata: { user_id: @user.id }
      })
    end

    test 'customer.created should create a customer' do
      customers = total_customers
      Webhook::CustomerEvent.new.created created_event[:data][:object]
      assert_equal customers + 1, total_customers
    end

    test 'customer.updated should update a customer' do
      data = created_event[:data][:object]
      customer = create(:customer, stripe_id: data[:id], user_id: @user.id)

      # Change the livemode value
      data[:livemode] = !data[:livemode]

      # Update the customer and assert the change
      Webhook::CustomerEvent.new.updated data
      assert_equal data[:livemode], customer.reload.livemode
    end

    test 'customer.deleted should remove a customer' do
      data = created_event[:data][:object]
      c = create(:customer, stripe_id: data[:id], user_id: @user.id)

      # Keep track of the current number of customers
      customers = total_customers

      # Send the delete event
      Webhook::CustomerEvent.new.deleted data

      # Assert that there is one less
      assert_equal customers - 1, total_customers
    end
  end
end

