require 'test_helper'

class StripeControllerTest < ActionDispatch::IntegrationTest

  test "response" do
    event = StripeMock.mock_webhook_event('customer.created')
    post stripe_path, { params: event.to_hash }
    assert_response :ok
  end

  test "Should raise an error with unknown events" do
    event = { type: "not.a.real.event" }
    post stripe_path, { params: event.to_hash }
    assert_response :bad_request
  end

  test "Should test webhooks" do
    event = StripeMock.mock_webhook_event('customer.created')
    post stripe_path, { params: event.to_hash }
    assert true
  end

  # test 'validate webhook secret' do
  #   SaasPayments.config.webhook_secret = 'whsec_test_secret'
  #   event = StripeMock.mock_webhook_event('customer.created')
  #   post stripe_path, { params: event.to_hash }
  #   assert_response :ok
  # end

  def assert_created event_type, resource
    total = send("total_#{resource.downcase}s")
    event = StripeMock.mock_webhook_event(event_type)
    post stripe_path, { params: event.to_hash }
    assert_equal total + 1, send("total_#{resource}s")
  end

  def assert_deleted resource, get_total
    event = StripeMock.mock_webhook_event("#{resource}.created")
    post stripe_path, { params: event.to_hash }
    total = send(get_total)
    event = StripeMock.mock_webhook_event("#{resource}.deleted")
    post stripe_path, { params: event.to_hash }
    assert_equal total - 1, send(get_total)
  end

  def assert_updated klass, resource, data
    event = StripeMock.mock_webhook_event("#{resource}.created")
    post stripe_path, { params: event.to_hash }
    obj = klass.find_by_stripe_id(event[:data][:object][:id])
    event = StripeMock.mock_webhook_event("#{resource}.updated", data)
    post stripe_path, { params: event.to_hash }
    yield obj.reload
  end

  test 'product.created' do
    assert_created "product.created", "product"
  end

  test 'product.deleted'  do
    assert_deleted "product", "total_products"
  end

  test 'product.updated' do
    assert_updated SaasPayments::Product, "product", name: 'Test' do |product|
      assert_equal "Test", product.name
    end
  end

  # Register Plan events
  test 'plan.created' do
    assert_created "plan.created", "plan"
  end

  test 'plan.deleted' do
    assert_deleted "plan", "total_plans"
  end

  test 'plan.updated' do
    assert_updated SaasPayments::Plan, "plan", nickname: 'Test' do |plan|
      assert_equal "Test", plan.nickname
    end
  end

  # Register Customer events
  test 'customer.created' do
    assert_created "customer.created", "customer"
  end

  test 'customer.deleted' do
    assert_deleted "customer", "total_customers"
  end

  test 'customer.updated' do
    assert_updated SaasPayments::Customer, "customer", name: 'Test' do |customer|
      assert_equal "Test", customer.name
    end
  end

  # Register Subscription events
  test 'customer.subscription.created' do
    assert_created "customer.subscription.created", "sub"
  end

  test 'customer.subscription.deleted' do
    assert_deleted "customer.subscription", "total_subs"
  end

  test 'customer.subscription.updated' do
    assert_updated SaasPayments::Subscription, "customer.subscription", livemode: true do |sub|
      assert_equal true, sub.livemode
    end
  end

  # Register Session events
  test 'checkout.session.completed' do
  end
end
