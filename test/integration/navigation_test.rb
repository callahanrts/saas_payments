require 'test_helper'

class NavigationTest < ActionDispatch::IntegrationTest
  test "should return ok" do
    get saas_payments_url
    assert_response :ok
  end
end
