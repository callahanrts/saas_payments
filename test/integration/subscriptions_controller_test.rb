require 'test_helper'

# This controller exists in the dummy app and uses the concerns provided by
# the saas_payments engine
class SubscriptionsControllerTest < ActionDispatch::IntegrationTest

  setup do
    @product = Stripe::Product.create(name: 'Mead Test')
    splan = stripe_helper.create_plan(product: @product)
    @plan = create(:plan, SaasPayments::Plan.from_stripe(splan))

    # annual plan
    aplan = stripe_helper.create_plan(id: 'test_plan_annual', nickname: 'test_annual', amount: 9999, interval: 'year', product: @product)
    @plan2 = create(:plan, SaasPayments::Plan.from_stripe(aplan))
  end

  def create_subscription options={}, user=nil, plan=@plan
    user ||= create(:user, email: '1@test.com', name: 'Joan')
    token = StripeMock.generate_card_token
    data = { user_id: user.id, plan_id: plan.id, stripeToken: token}
    post subscriptions_path, { params: data }.merge(options)
    user
  end


  # Creating a subscription
  # =======================

  test "should create a customer" do
    customers = total_customers
    create_subscription
    assert_equal customers + 1, total_customers
  end

  test "should not create duplicate customers" do
    # It will create a customer initially
    u = create_subscription

    customers = total_customers

    # Try again with the same user and plan
    token = StripeMock.generate_card_token
    post subscriptions_path, params: { user_id: u.id, plan_id: @plan.id, stripeToken: token}

    # A duplicate customer should not be created
    assert_equal customers, total_customers
  end

  test "should create a subscription" do
    customers = total_customers
    u = create_subscription

    sub = u.reload.customer.subscription
    assert_equal @plan.stripe_id, sub.plan_id
  end

  # Updating a subscription
  # =======================
  test 'should change the plan connected to a subscription' do
    u = create_subscription
    post plan_subscriptions_path, params: { user_id: u.id, plan_id: @plan2.id }
    sub = u.reload.customer.subscription(remote: true)
    assert_equal @plan2.stripe_id, sub[:items][:data][0][:plan][:id]
  end


  # Updating a subscription -- through create
  # =========================================

  test "should not create duplicate subscriptions" do
    u = create(:user, email: '2@test.com', name: 'Time')
    create_subscription({}, u)
    create_subscription({}, u)

    subs = Stripe::Subscription.list
      .map{|s| s[:customer]}
      .select{|c| c == u.customer.stripe_id}

    assert_equal 1, subs.length
  end

  test "should up/downgrade the plan if the plan has changed" do
    u = create(:user, email: '2@test.com', name: 'Time')
    splan = Stripe::Plan.create(product: @product, amount: 989, currency: 'usd', interval: 'monthly')
    plan = create(:plan, SaasPayments::Plan.from_stripe(splan))
    create_subscription({}, u)
    create_subscription({}, u, plan)

    assert_equal plan.stripe_id, u.reload.customer.subscription.plan_id
  end


  # Deleting a subscription
  # =======================

  test 'should set cancel_at_period_end when the user cancels' do
    u = create_subscription
    post cancel_subscriptions_path, params: { user_id: u.id }
    sub = u.reload.customer.subscription(remote: true)
    assert_equal true, sub[:cancel_at_period_end]
  end

  test 'should cancel a subscription immediately' do
    u = create_subscription
    post cancel_subscriptions_path, params: { user_id: u.id, cancel_now: true }
    sub = u.reload.customer.subscription(remote: true)
    assert_equal "canceled", sub.status
  end

  # Resuming a subscription
  # =======================

  test 'should resume a subscription that is scheduled to cancel' do
    u = create_subscription
    post cancel_subscriptions_path, params: { user_id: u.id }
    post resume_subscriptions_path, params: { user_id: u.id }
    sub = u.reload.customer.subscription(remote: true)
    assert_equal false, sub[:cancel_at_period_end]
  end


  # HTML Responses
  # =======================

  test '(html) success returns 200' do
    create_subscription
    assert_redirected_to SaasPayments.config.account_path
  end

  test '(html) card failed returns 402 + message' do
    StripeMock.prepare_card_error(:card_declined, :create_subscription)

    create_subscription({headers: { "HTTP_REFERER" => "/back" } })
    assert_redirected_to '/back'
  end

  test '(html) customer action returns 200 + message' do
  end



  # JSON Responses
  # =======================

  test '(json) success returns 200' do
    create_subscription({ as: :json })
    assert_response :ok
    assert_equal "success", JSON.parse(@response.body)['message']
  end

  test '(json) card failed returns 402 + message' do
    StripeMock.prepare_card_error(:card_declined, :create_subscription)

    create_subscription({ as: :json })
    assert_response :bad_request
  end

  test '(json) customer action returns 200 + message' do
  end
end
