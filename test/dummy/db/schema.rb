# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_08_24_162100) do

  create_table "saas_payments_customers", force: :cascade do |t|
    t.integer "user_id"
    t.string "stripe_id"
    t.boolean "delinquent"
    t.string "description"
    t.text "discount"
    t.string "email"
    t.boolean "livemode"
    t.text "metadata"
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["stripe_id"], name: "index_saas_payments_customers_on_stripe_id", unique: true
    t.index ["user_id"], name: "index_saas_payments_customers_on_user_id"
  end

  create_table "saas_payments_plans", force: :cascade do |t|
    t.string "stripe_id"
    t.string "product_id"
    t.boolean "active"
    t.integer "amount"
    t.string "amount_decimal"
    t.string "currency"
    t.string "interval"
    t.integer "interval_count"
    t.boolean "livemode"
    t.text "metadata"
    t.string "nickname"
    t.integer "trial_period_days"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["stripe_id", "product_id"], name: "index_saas_payments_plans_on_stripe_id_and_product_id", unique: true
    t.index ["stripe_id"], name: "index_saas_payments_plans_on_stripe_id", unique: true
  end

  create_table "saas_payments_products", force: :cascade do |t|
    t.string "stripe_id"
    t.boolean "active"
    t.string "caption"
    t.boolean "livemode"
    t.text "metadata"
    t.string "name"
    t.string "type"
    t.string "unit_label"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["stripe_id"], name: "index_saas_payments_products_on_stripe_id", unique: true
  end

  create_table "saas_payments_subscriptions", force: :cascade do |t|
    t.string "plan_id"
    t.string "customer_id"
    t.string "stripe_id"
    t.datetime "cancel_at"
    t.datetime "canceled_at"
    t.datetime "current_period_end"
    t.datetime "current_period_start"
    t.boolean "cancel_at_period_end"
    t.boolean "livemode"
    t.text "metadata"
    t.datetime "start"
    t.datetime "start_date"
    t.string "status"
    t.datetime "trial_end"
    t.datetime "trial_start"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["customer_id"], name: "index_saas_payments_subscriptions_on_customer_id"
    t.index ["stripe_id"], name: "index_saas_payments_subscriptions_on_stripe_id", unique: true
  end

  create_table "users", force: :cascade do |t|
    t.string "name"
    t.string "email"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
