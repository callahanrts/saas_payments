class SubscriptionsController < ApplicationController
  include SaasPayments::SubscriptionConcerns

  before_action :get_current_user
  before_action :get_plan, only: [:create, :plan]

  def create
    sign_up_for_plan @current_user, @plan
  end

  def cancel
    if params[:cancel_now]
      cancel_now @current_user
    else
      cancel_at_period_end @current_user
    end
  end

  def plan
    change_plan @current_user, @plan
  end

  def resume
    resume_subscription @current_user
  end

  private

  def get_plan
    @plan = SaasPayments::Plan.find(params.require(:plan_id))
  end

  # This would authenticate a user in a real application
  def get_current_user
    @current_user = User.find(params.require(:user_id))
  end

end
