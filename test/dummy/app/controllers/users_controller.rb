class UsersController < ApplicationController
  def index
    @users = User.all
  end

  def show
    @user = User.find(params[:id])
    @plans = SaasPayments::Plan.all
  end

  def single
    @user = User.find(params[:id])
    @plans = SaasPayments::Plan.all
  end

  def multiple
    @user = User.find(params[:id])
    @plans = SaasPayments::Plan.all
  end
end
