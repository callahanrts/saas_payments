class User < ApplicationRecord
  has_one :customer, class_name: "SaasPayments::Customer"
end
