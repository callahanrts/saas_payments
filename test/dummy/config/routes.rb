Rails.application.routes.draw do
  mount SaasPayments::Engine => "/saas_payments"
  root 'users#index'
  resources :users, only: [:index, :show] do
    member do
      get :single
      get :multiple
    end
  end
  resources :subscriptions, only: [:create] do
    collection do
      post :cancel
      post :plan
      post :resume
    end
  end

  post "stripe", to: "stripe#create"
end
