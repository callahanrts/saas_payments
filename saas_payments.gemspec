$:.push File.expand_path("lib", __dir__)

# Maintain your gem's version:
require "saas_payments/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |spec|
  spec.name        = "saas_payments"
  spec.version     = SaasPayments::VERSION
  spec.authors     = ["Cody"]
  spec.email       = ["callahanrts@gmail.com"]
  spec.homepage    = "https://github.com/callahanrts/saas_payments"
  spec.summary     = "Manage plans for your SAAS app"
  spec.description = "Manage plans for your SAAS app"
  spec.license     = "MIT"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  # if spec.respond_to?(:metadata)
  #   spec.metadata["allowed_push_host"] = "TODO: Set to 'http://mygemserver.com'"
  # else
  #   raise "RubyGems 2.0 or newer is required to protect against " \
  #     "public gem pushes."
  # end

  spec.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  spec.add_dependency "rails", "~> 5.2.3"
  spec.add_dependency "stripe", "~> 4.24.0"

  spec.add_development_dependency "sqlite3"
  spec.add_development_dependency 'stripe-ruby-mock', '~> 2.5.8'
  spec.add_development_dependency 'database_cleaner'
  spec.add_development_dependency 'factory_bot_rails'
  spec.add_development_dependency 'simplecov'
end
